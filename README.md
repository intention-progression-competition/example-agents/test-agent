This is a dumb client, used solely for the purpose of passing XML commands to the local BDI client. 



Sample commands are as follows: 

 ```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><initiate><seed>10000</seed></initiate></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T3-A155</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T2-A62</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T3-A124</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T3-A125</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T3-A126</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T3-A127</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><action>T3-A128</action></command>
```
```
<?xml version="1.0" encoding="UTF-8"?><command clientid="UONTEST1"><quit></quit></command>
```
