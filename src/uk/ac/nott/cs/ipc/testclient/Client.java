// ----------------------------------------------------------------------------
// Copyright (C) 2019 IPC Organising Committee
//
// This file is part of the IPC Contest Software.
// 
// The IPC Contest Software is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
// 
// The IPC Contest Software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public
// License along with the IPC Contest Software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
// 
// To contact the authors:
// https://www.intentionprogression.org/contact/
//
//----------------------------------------------------------------------------

package uk.ac.nott.cs.ipc.testclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {

    public static void main(String[] args) throws IOException {

        String simulatorHostName = "127.0.0.1";
        int BDIPortNumber = 40000;

        boolean isTestingStack = false;
        TestMode testMode = new TestMode();

        int i = 0;
        String arg;

        while(i < args.length && args[i].startsWith("-")){
            arg = args[i++];
            if(arg.length() != 2) {
                System.out.println(arg + " is not a valid flag");
                System.exit(1);
            }

            char flag = arg.charAt(1);
            switch (flag) {
                case 'L': // BDI port
                    BDIPortNumber = Integer.parseInt(args[i++]);
                    break;

                case 't':
                    System.out.println("TEST MODE ENABLED");
                    isTestingStack = true;
                    break;
                    
                case 'C':
					System.out.println("CONTAINERISED SOLUTION MODE ENABLED");
					simulatorHostName = "ipc_server";
					break;
            }
        }

        Socket socket = new Socket(simulatorHostName, BDIPortNumber);
        PrintWriter serverOut = new PrintWriter(socket.getOutputStream(), true);
        BufferedReader serverIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        try {
            BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
            String serverResponse;
            String userCommand;

            System.out.println("Connection to BDI established on port " + BDIPortNumber);

            do {

                if (isTestingStack) {
                    userCommand = testMode.getNextCommand();
                } else {
                    userCommand = stdIn.readLine();
                }

                serverOut.println(userCommand);

                serverResponse = serverIn.readLine();

                if (isTestingStack) {
                    if (testMode.checkResponseForCompletion(serverResponse)) {
                        System.out.println("The stack appear to be configured correctly and the test domain completed with success.");
                        System.exit(0);
                    }
                } else {
                    System.out.println(serverResponse);
                }


                if (testMode.isTestingComplete()) {
                    System.out.println("The stack does not appear to be configured correctly as the test domain did not complete.");
                    System.exit(1);
                }

                if (serverResponse.equals("quit")) {
                    System.exit(1);
                }

            } while (serverResponse != null);

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + simulatorHostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " + simulatorHostName);
            System.exit(1);
        }
    }
}
