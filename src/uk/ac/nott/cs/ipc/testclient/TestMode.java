package uk.ac.nott.cs.ipc.testclient;

public class TestMode {

    private String[] testCommands;

    private Integer nextIndex = 0;

    public TestMode() {

        testCommands = new String[]{
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><initiate><seed>1000</seed><gptfile>gpts/test-gpts/gpt-testclient.xml</gptfile></initiate></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A0</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A1</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A2</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A3</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A4</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A5</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A6</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T1-A7</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A0</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A1</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A2</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A3</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A4</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A5</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A6</action></command>",
        "<?xml version=\"1.0\" encoding=\"UTF-8\"?><command clientid=\"UONTEST1\"><action>T0-A7</action></command>"};
        
    }

    public String getNextCommand() {
        return this.testCommands[this.nextIndex++];
    }

    public boolean isTestingComplete() {
        return nextIndex >= testCommands.length;
    }

    public boolean checkResponseForCompletion(String serverResponse) {
        String matchingCondition = "<contest>COMPLETE</contest>";
		System.out.println("Server response: "+serverResponse);
        return serverResponse.toLowerCase().contains(matchingCondition.toLowerCase());
    }
}
